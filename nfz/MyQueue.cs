﻿namespace nfz
{
    internal class MyQueue
    {
        private class Node
        {
            public Node(AppointmentDoctor data, Node? next = null)
            {
                this.Data = data;
                this.Next = next ?? this;
            }

            public AppointmentDoctor Data;
            public Node? Next;
        }

        private Node? _head;
        private Node? _tail;

        public bool IsEmpty()
        {
            return _head == null || _tail == null;
        }

        public void Enqueue(AppointmentDoctor item)
        {
            if (IsEmpty())
            {
                _tail = _head = new Node(item);
            }
            else
            {
                _tail = _tail!.Next = new Node(item, _tail.Next);
            }
        }

        public AppointmentDoctor? Peek(int skip = 0)
        {
            if (IsEmpty() || skip < 0) return null;
            Node currentNode = _head!;
            for (int i = 0; i < skip; ++i)
            {
                currentNode = currentNode.Next!;
                if (currentNode == _head) break;
            }

            return currentNode!.Data;
        }

        public AppointmentDoctor? PeekTail()
        {
            return IsEmpty() ? null : _tail!.Data;
        }

        public AppointmentDoctor? Dequeue()
        {
            if (IsEmpty()) return null;
            var toReturn = _head!.Data;
            if (_head.Next == _head)
            {
                _tail=_head = null;
            }
            else
            {
                _tail.Next=_head = _head.Next;
                
            }

            return toReturn;
        }

        public void Edit(AppointmentDoctor value)
        {
            if (!IsEmpty())
            {
                _head!.Data = value;
            }
        }

        public void Skip()
        {
            _tail = _head;
            if (!IsEmpty()) _head = _head!.Next;
        }
    }
}