﻿using System;

namespace nfz
{
    public struct AppointmentDoctor
    {
        public AppointmentDoctor(string newName,string newSurgery,DateTime newDateTime)
        {
            name = newName;
            surgery = newSurgery;
            dateTime= newDateTime;
        }
        public string name;
        public string surgery;
        public DateTime dateTime;
    }
}