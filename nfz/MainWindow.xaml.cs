﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace nfz
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MyQueue appointments = new MyQueue();
        private DispatcherTimer timerToRefreshTime = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();
            reloadCurrentDateAndTime();
            timerToRefreshTime.Interval = new TimeSpan(0, 0, 10);
            timerToRefreshTime.Tick += timerToRefreshTime_Tick;
            timerToRefreshTime.Start();
        }

        private void reloadCurrentDateAndTime()
        {
            txtCurrentDate.Content = DateTime.Now.Date.ToString("D");
            timerToRefreshTime_Tick(null, null);
        }

        private void timerToRefreshTime_Tick(object sender, EventArgs e)
        {
            txtCurrentTime.Content = DateTime.Now.ToString("T");
        }

        private void ButtonSaveAsNewPatient_click_Click(object sender, RoutedEventArgs e)
        {
            appointments.Enqueue(
                new AppointmentDoctor(txtCurrentPatientName.Text,
                    txtCurrentPatientSurgery.Text,
                    dtpCurrentPatientDate.SelectedDate.Value));
        }

        private void ButtonNext_OnClick(object sender, RoutedEventArgs e)
        {
            if (CheckIsThereAreAppointmentsAndIfNoTellUser()) return;
            appointments.Skip();
            DisplayCurrentAppointment();
            DisplayTailInPreviousPatientForm();
            DisplayNextPatient();
            DisplayHowManyDaysToAppointment();
        }

        private void DisplayCurrentAppointment()
        {
            AppointmentDoctor appointment = (AppointmentDoctor) appointments.Peek();
            txtCurrentPatientName.Text = appointment.name;
            dtpCurrentPatientDate.SelectedDate = appointment.dateTime;
            txtCurrentPatientSurgery.Text = appointment.surgery;
        }

        private bool CheckIsThereAreAppointmentsAndIfNoTellUser()
        {
            if (appointments.IsEmpty())
            {
                MessageBox.Show("There are no added appointments yet!");
                return true;
            }
            else return false;
        }

        private void DisplayTailInPreviousPatientForm()
        {
            AppointmentDoctor appointment = (AppointmentDoctor) appointments.PeekTail();
            TextPreviousPatientName.Text = appointment.name;
            TextPreviousPatientSurgery.Text = appointment.surgery;
            DatePickerPreviousPatientAppointment.SelectedDate = appointment.dateTime;
        }

        private void ButtonDelete_OnClick(object sender, RoutedEventArgs e)
        {
            appointments.Dequeue();
            DisplayCurrentAppointment();
        }
        
        private void DisplayNextPatient()
        {
            AppointmentDoctor appointment = (AppointmentDoctor) appointments.Peek(1);
            TextBoxNextPatientName.Text = appointment.name;
            TextBoxNextPatientSurgery.Text = appointment.surgery;
            DatePickerNextPatientAppointment.SelectedDate = appointment.dateTime;
        }

        private void DisplayHowManyDaysToAppointment()
        {
            AppointmentDoctor appointment = (AppointmentDoctor) appointments.Peek();
            int daysToAppointment = (appointment.dateTime - DateTime.Now).Days;
            LabelDaysToAppointment.Content = daysToAppointment;
            LabelDaysToAppointment.FontWeight = daysToAppointment > 0 ? FontWeights.Bold : FontWeights.Normal;
            LabelDaysToAppointment.Foreground = daysToAppointment == 0 ? Brushes.Red : Brushes.Black;
        }
    }
}